package com;


import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        Operation op = new Operation();

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Operation ( + , -, *, /): " );
        String operation =  scan.nextLine();
        System.out.println("Enter the first value (a): " );
        int a = scan.nextInt();
        System.out.println("Enter the first value (b): ");
        int b = scan.nextInt();

        int result = op.Operation(a,b,operation);
        System.out.println("Result of " + operation + " on a,b is " + result );
    }
}
